import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {JwtHelper} from "angular2-jwt";
import {User} from "../../models/User";
import {Response} from "../../interfaces/response";

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  private headers: Headers;
  private apiUrl: String = 'http://localhost:3100';
  private token: string;
  private user: User;
  private jwtHelper: JwtHelper;

  constructor(private http: Http) {
    this.jwtHelper = new JwtHelper();
  }

  login(credentials: any): Observable<Response> {
    this.setHeaders();

    return this.http.post(localStorage.getItem('url_http') + '/login', credentials, {headers: this.headers})
      .map(response => response.json())
      .catch((error: any) => Observable.throw(error.json()));
  }

  getProfile(): Observable<Response> {
    this.setHeaders();
    this.loadToken();
    this.loadUser();
    this.setAuthHeader(this.token);

    return this.http.get(localStorage.getItem('url_http') + '/users/' + this.user.username, {headers: this.headers})
      .map(response => response.json())
      .catch((error: any) => Observable.throw(error.json()));
  }

  saveUserInfo(data): void {
    localStorage.setItem('token', data.token);
    localStorage.setItem('user', JSON.stringify(data.user));
  }

  getLoggedInUser(): User {
    this.loadUser();
    return this.user;
  }

  loggedIn(): boolean {
    this.loadToken();

    return this.token ? !this.jwtHelper.isTokenExpired(this.token) : false;
  }

  logout(): void {
    this.token = null;
    this.user = null;

    localStorage.clear();
  }

  private loadToken(): void {
    this.token = localStorage.getItem('token');
  }

  private loadUser(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  private setHeaders() {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', '*');
  }

  private setAuthHeader(authToken: string) {
    this.headers.append('Authorization', authToken);
  }

}
