import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import {Response} from "../../interfaces/response";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";

/*
  Generated class for the LogsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LogsProvider {

  private headers: Headers;

  constructor(private http: Http) {
  }

  create(logs: any): Observable<Response> {
    this.setHeaders();

    return this.http.post(localStorage.getItem('url_http') + '/logs', logs, {headers: this.headers})
      .map(response => response.json())
      .catch((error: any) => Observable.throw(error.json()));
  }

  all(): Observable<Response> {
    this.setHeaders();

    return this.http.get(localStorage.getItem('url_http') + '/logs', {headers: this.headers})
      .map(response => response.json())
      .catch((error: any) => Observable.throw(error.json()));
  }

  private setHeaders() {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', '*');
  }

  private setAuthHeader(authToken: string) {
    this.headers.append('Authorization', authToken);
  }

}
