import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import { Socket } from 'ng-socket-io';
import {Subscription} from "rxjs/Subscription";

/**
 * Generated class for the ManualPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-manual',
  templateUrl: 'manual.html',
})
export class ManualPage {

  buttons: Array<boolean> = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  subscription: Subscription;
  statusMessage: string;
  weightMessage: string;
  percentageMessage: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private socket: Socket,
    private alertController: AlertController
    ) {
    this.subscription = this.getStatus().subscribe((message: string) => {
      if (message.indexOf('porcentaje') >= 0) {
        this.percentageMessage = message;
      } else if (message.indexOf('peso') >= 0) {
        this.weightMessage = message;
      } else {
        this.statusMessage = message
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ManualPage');
  }

  private getStatus() {
    return this.socket
      .fromEvent("status")
      .map( data => data );
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  onButtonClick(i: number): void {
    if (i == 0) {
      if (this.buttons[0] == true){
        this.socket.emit('manual', 'A\n');
      } else {
        this.socket.emit('manual', 'C\n');
      }
    }

    if (i == 1) {
      this.socket.emit('manual', 'B\n');
    }

    if (i == 2) {
      if (this.buttons[2] == true){
        this.socket.emit('manual', 'D\n');
      } else {
        this.socket.emit('manual', 'F\n');
      }
    }

    if (i == 3) {
      this.socket.emit('manual', 'E\n');
    }

    if (i == 4) {
      if (this.buttons[4] == true){
        this.socket.emit('manual', 'G\n');
      } else {
        this.socket.emit('manual', 'I\n');
      }
    }

    if (i == 5) {
      this.socket.emit('manual', 'H\n');
    }

    if (i == 6) {
      this.socket.emit('manual', 'J\n');
    }

    if (i == 7) {
      this.socket.emit('manual', 'K\n');
    }

    if (i == 8) {
      if (this.buttons[8] == true){
        this.socket.emit('manual', 'L\n');
      } else {
        this.socket.emit('manual', 'M\n');
      }
    }

    if (i == 9) {
      if (this.buttons[9] == true){
        this.socket.emit('manual', 'N\n');
      } else {
        this.socket.emit('manual', 'O\n');
      }
    }

    if (i == 10) {

      let confirm = this.alertController.create({
        title: 'Parada de Emergencia',
        message: 'Seguro que quieres detener el proceso',
        buttons: [
          {
            text: 'Cancelar',
            handler: () => {}
          },
          {
            text: 'Seguro!',
            handler: () => {
              let confirm = this.alertController.create({
                title: 'Parada de Emergencia',
                message: 'Está realmente seguro de que quieres detener el proceso',
                buttons: [
                  {
                    text: 'Cancelar',
                    handler: () => {}
                  },
                  {
                    text: 'Seguro!',
                    handler: () => {
                      this.socket.emit('manual', 'P\n');

                      for (let i = 0; i < this.buttons.length; i++) {
                        this.buttons[i] = false;
                      }
                    }
                  }
                ]
              });
              confirm.present();
            }
          }
        ]
      });
      confirm.present();
    }
  }

}
