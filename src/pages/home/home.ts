import { Component } from '@angular/core';
import {AlertController, MenuController, NavController} from 'ionic-angular';
import { Socket } from 'ng-socket-io';
import {StartPage} from "../start/start";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  statusMessage;
  pauseButtonMessage: string = "Pausar";
  subscription: Subscription;
  values: Array<String>;

  constructor(
    public navCtrl: NavController,
    private menuController: MenuController,
    private socket: Socket,
    private alertController: AlertController
  ) {
    this.socket.connect();
    if (localStorage.getItem('values')) {
      this.values = localStorage.getItem('values').split('#');
    }

    this.subscription = this.getStatus().subscribe((message: string) => {
      if (message.indexOf("#") >= 0) {
        this.values = message.split("#");
        localStorage.setItem("values", message);
      } else if (message.indexOf("excedió") >= 0) {
        let confirm = this.alertController.create({
          title: 'Alerta!',
          subTitle: message,
          buttons: ['OK']
        });
        confirm.present();
      } else {
        this.statusMessage = message
      }
    });
  }

  ionViewDidLoad() {
    this.menuController.enable(true, 'main-menu');
  }

  private getStatus() {
    return this.socket
      .fromEvent("status")
      .map( data => data );
  }

  onStart(): void {
    if (this.statusMessage == "Continuar?") {
      this.socket.emit('start', "A");
    } else {
      this.navCtrl.setRoot(StartPage);
    }
  }

  onPause(): void {
    if(this.pauseButtonMessage === "Pausar") {
      this.socket.emit('pause', "B");
      this.pauseButtonMessage = "Continuar"
    } else {
      this.socket.emit('start', "A");
      this.pauseButtonMessage = "Pausar"
    }
  }

  onStop(): void {
    let confirm = this.alertController.create({
      title: 'Parada de Emergencia',
      message: 'Seguro que quieres detener el proceso',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {}
        },
        {
          text: 'Seguro!',
          handler: () => {
            let confirm = this.alertController.create({
              title: 'Parada de Emergencia',
              message: 'Está realmente seguro de que quieres detener el proceso',
              buttons: [
                {
                  text: 'Cancelar',
                  handler: () => {}
                },
                {
                  text: 'Seguro!',
                  handler: () => {
                    this.socket.emit('stop', "C");
                  }
                }
              ]
            });
            confirm.present();
          }
        }
      ]
    });
    confirm.present();
  }

  notWorking(): boolean {
    if ((this.statusMessage && this.statusMessage.indexOf('Esperando') >= 0) ||
      (this.statusMessage && this.statusMessage.indexOf('PARADA') >= 0)) {
    }
    return (this.statusMessage && this.statusMessage.indexOf('Esperando') >= 0) ||
      (this.statusMessage && this.statusMessage.indexOf('PARADA') >= 0);
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }
}
