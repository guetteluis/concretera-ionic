import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {LogsProvider} from "../../providers/logs/logs";
import {HomePage} from "../home/home";

/**
 * Generated class for the LogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-log',
  templateUrl: 'log.html',
})
export class LogPage {

  log: Array<Object>;
  loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private logsProvider: LogsProvider,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) {
    this.loading = this.loadingController.create({
      content: 'Por favor espere...'
    });
  }

  ionViewDidLoad() {
    this.loading.present();
    this.logsProvider.all().subscribe((response) => {
      this.log = response.data;
      this.loading.dismiss();
    }, (error) => {
      this.loading.dismiss();
      let alert = this.alertController.create({
        title: 'Logs',
        subTitle: "Error de comunicación",
        buttons: ['OK']
      });
      alert.present();
      this.navCtrl.setRoot(HomePage);
    })
  }

}
