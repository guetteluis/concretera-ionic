import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, MenuController, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthProvider} from "../../providers/auth/auth";
import {HomePage} from "../home/home";
import {SettingsPage} from "../settings/settings";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  loading;
  alert;

  constructor(
    public navController: NavController,
    public navParams: NavParams,
    private menuController: MenuController,
    private formBuilder: FormBuilder,
    private auth: AuthProvider,
    private loadingController: LoadingController,
    private alertController: AlertController
    ) {

    if (!localStorage.getItem('url_http') || !localStorage.getItem('url_websocket')) {
      this.navController.setRoot(SettingsPage);
    }

    if (this.auth.loggedIn()) {
      this.navController.setRoot(HomePage);
    }

    this.loading = this.loadingController.create({
      content: 'Por favor espere...'
    });

    this.buildLoginForm();
  }

  ionViewDidLoad() {
    this.menuController.enable(false, 'main-menu');
  }

  private buildLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      'username': ['',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(20),
          Validators.pattern('[a-zA-Z0-9\-]+'),
        ]
      ],
      'password': ['',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),
          Validators.pattern('^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.\\-_*])([a-zA-Z0-9@#$%^&+=*.\\-_]){3,}$')
        ]
      ]
    });
  }

  loginSubmit(): void {
    this.loading.present();
    const formControls = this.loginForm.controls;
    const credentials = {
      username: formControls['username'].value,
      password: formControls['password'].value
    };

    this.auth.login(credentials).subscribe((response) => {

      this.auth.saveUserInfo(response.data);
      this.loading.dismiss();
      this.navController.setRoot(HomePage);

    }, (error) => {
      let message = 'Error de comunicación';

      if (error.message == 'user_not_found'){
        message = 'Usuario no encontrado'
      } else if (error.message == 'wrong_password'){
        message = 'Contraseña errónea'
      } else {
        this.navController.setRoot(SettingsPage);
      }

      this.loading.dismiss();
      this.alert = this.alertController.create({
        title: 'Login',
        subTitle: message,
        buttons: ['OK']
      });
      this.alert.present();
    });
  }
}
