import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HomePage} from "../home/home";
import {AuthProvider} from "../../providers/auth/auth";
import {LoginPage} from "../login/login";

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  url: String;
  settingsForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private alertController: AlertController,
    private auth: AuthProvider
    ) {
    this.buildSettingsForm();
  }

  ionViewDidLoad() {}

  private buildSettingsForm(): void {
    this.settingsForm = this.formBuilder.group({
      'url_http': [localStorage.getItem('url_http'),
        [
          Validators.required,
          Validators.pattern('((http|https):\\/\\/)[-a-zA-Z0-9:%._\\+~#=]{2,256}[\\.a-z0-9]{2,6}([-a-zA-Z0-9:%_\\+.~#?&//=]*)'),
        ]
      ],
      'url_websocket': [localStorage.getItem('url_websocket'),
        [
          Validators.required,
        ]
      ]
    });
  }

  settingsSubmit(): void {
    localStorage.setItem("url_http", this.settingsForm.controls['url_http'].value);
    localStorage.setItem("url_websocket", this.settingsForm.controls['url_websocket'].value);

    let alert = this.alertController.create({
      title: 'Configuración',
      subTitle: 'Los datos han sido guardados',
      buttons: ['OK']
    });
    alert.present();

    if (this.auth.loggedIn()) {
      this.navCtrl.setRoot(HomePage);
    } else {
      this.navCtrl.setRoot(LoginPage);
    }
  }

}
