import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {minValue} from "../../directives/number-validators/min-value-validator.directive";
import {maxValue} from "../../directives/number-validators/max-value-validator.directive";
import { Socket } from 'ng-socket-io';
import {HomePage} from "../home/home";
import {LogsProvider} from "../../providers/logs/logs";
import {AuthProvider} from "../../providers/auth/auth";

/**
 * Generated class for the StartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class StartPage {

  startForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private socket: Socket,
    private alertController: AlertController,
    private logsProvider: LogsProvider,
    private auth: AuthProvider
    ) {
    this.buildStartForm();
  }

  ionViewDidLoad() {
  }


  private buildStartForm(): void {
    this.startForm = this.formBuilder.group({
      'arena': ['',
        [
          Validators.required,
          minValue(1),
          maxValue(95)
        ]
      ],
      'cemento': ['',
        [
          Validators.required,
          minValue(1),
          maxValue(95)
        ]
      ],
      'piedra': ['',
        [
          Validators.required,
          minValue(1),
          maxValue(95)
        ]
      ],
      'agua_aditivo': ['',
        [
          Validators.required,
          minValue(1),
          maxValue(95)
        ]
      ],
      'agua': ['',
        [
          Validators.required,
          minValue(1),
          maxValue(95)
        ]
      ]
    });
  }

  startSubmit(): void {
    const formControls = this.startForm.controls;
    const data = {
      _user: this.auth.getLoggedInUser()._id,
      arena: formControls['arena'].value,
      cemento: formControls['cemento'].value,
      piedra: formControls['piedra'].value,
      agua_aditivo: formControls['agua_aditivo'].value,
      agua: formControls['agua'].value,
    };

    if (parseInt(data.arena) + parseInt(data.cemento) + parseInt(data.piedra) +
      parseInt(data.agua_aditivo) + parseInt(data.agua) == 100) {

      const stringData = data.arena + "," + data.cemento + "," + data.piedra + "," + data.agua_aditivo + "," + data.agua + "\n";
      this.socket.emit('start', stringData);

      this.logsProvider.create(data).subscribe((response) => {
        this.navCtrl.setRoot(HomePage);
      });

    } else {
      let alert = this.alertController.create({
        title: 'Iniciar Proceso',
        subTitle: 'La sumatoria de los ingredientes debe ser igual a 100%',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  ionViewWillLeave() {
  }
}
