import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { LoginPage } from "../pages/login/login";
import { HomePage } from '../pages/home/home';
import { StartPage } from "../pages/start/start";
import { SettingsPage } from "../pages/settings/settings";
import { LogPage } from "../pages/log/log";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
import { HttpModule } from "@angular/http";
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { LogsProvider } from '../providers/logs/logs';
import {ManualPage} from "../pages/manual/manual";

const config: SocketIoConfig = { url: localStorage.getItem('url_websocket'), options: {} };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    StartPage,
    SettingsPage,
    LogPage,
    ManualPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    SocketIoModule.forRoot(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    StartPage,
    SettingsPage,
    LogPage,
    ManualPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    LogsProvider
  ]
})
export class AppModule {}
